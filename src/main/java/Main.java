import com.github.javafaker.Faker;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import database.EmployeeRepository;
import domain.Address;
import domain.Employee;

import java.util.List;
import java.util.Optional;

public class Main {

    private static EmployeeRepository repository;

    public  static void main (String [] args) {
        String connectionURL = "mongodb://tomaz:mongodb_password@localhost:27017";
        try (MongoClient client = MongoClients.create(connectionURL)) {
            repository = new EmployeeRepository(client);
            String toBeDeletedId = insertEmployee(repository);
            String toBeUpdatedId = insertEmployee(repository);
            String toBeRetrieved = insertEmployee(repository);

            showSingleEmployee(toBeRetrieved);
            showAllEmployees();
            updateEmployeeName(toBeUpdatedId);
            deleteEmployee(toBeDeletedId);
        } catch (Exception exception) {
            System.out.println("Occurred an error." + exception.getMessage());
        }
    }

    public static String insertEmployee (EmployeeRepository repository) {
        Faker faker = new Faker();
        Address address = new Address(
                faker.address().streetName(),
                faker.number().numberBetween(10, 500),
                faker.address().lastName(),
                faker.address().city(),
                faker.address().state(),
                faker.address().country(),
                faker.address().zipCode()
        );

        Employee employee = new Employee(
                faker.name().fullName(),
                faker.phoneNumber().phoneNumber(),
                address,
                faker.random().nextBoolean()
        );

        return repository.insert(employee);
    }

    public static void showSingleEmployee(String id) {
        Optional<Employee> employee = repository.getById(id);
        if(employee.isEmpty()) {
            System.out.println("Employee with id " + id + "was not found.");
            return;
        }

        System.out.println("***************************** SHOWING SINGLE EMPLOYEE INFO *****************************\n");
        System.out.println(employee.get());
        System.out.println("***************************** END OF SINGLE EMPLOYEE INFO *****************************\n");
    }

    public static void showAllEmployees () {
        List<Employee> employees = repository.getAll();

        System.out.println("***************************** SHOWING ALL EMPLOYEE INFO *****************************\n");
        employees.forEach(e -> System.out.println(e.toString()));
        System.out.println("***************************** END OF ALL EMPLOYEE INFO ******************************\n");
    }

    public static void updateEmployeeName (String id) {
        Optional<Employee> employee = repository.getById(id);
        if(employee.isEmpty()) {
            System.out.println("Employee with id " + id + "was not found.");
            return;
        }

        String oldName = employee.get().getName();
        String newName = Faker.instance().name().fullName();
        employee.get().setName(newName);

        repository.update(employee.get());
        Optional<Employee> updated = repository.getById(id);

        if(updated.isPresent()) {
            System.out.println("******************** SHOWING UPDATED EMPLOYEE INFO ********************\n");
            System.out.println("Employee old name: " + oldName + "\n");
            System.out.println(updated.get());
            System.out.println("********************* END OF UPDATED EMPLOYEE INFO ********************\n");
        }
    }

    public static void deleteEmployee (String id) {
        repository.deleteById(id);
    }
}
