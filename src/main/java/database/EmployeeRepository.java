package database;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.InsertOneResult;
import domain.Address;
import domain.Employee;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class EmployeeRepository {
    private MongoCollection<Document> collection;

    public EmployeeRepository (MongoClient client) {
        collection = client
                .getDatabase("database_assignment")
                .getCollection("employee");
    }

    public String insert(Employee employee) {
        Document employeeDoc = parseEmployeeToDocument(employee);
        InsertOneResult result = this.collection.insertOne(employeeDoc);

        return Objects.requireNonNull(result.getInsertedId())
                .asObjectId()
                .getValue()
                .toString();
    }

    public List<Employee> getAll() {
        return this.collection.find()
                .map(this::parseDocumentToEmployee)
                .into(new ArrayList<>());
    }

    public Optional<Employee> getById(String id) {
        Bson filter = Filters.eq("_id", new ObjectId(id));
        Document employeeDoc = this.collection.find(filter).first();
        if(employeeDoc == null) {
            return Optional.empty();
        }

        return Optional.of(parseDocumentToEmployee(employeeDoc));
    }

    public void deleteById(String id) {
        Bson filter = Filters.eq("_id", new ObjectId(id));
        boolean isDeleted = this.collection.findOneAndDelete(filter) != null;

        if(isDeleted) {
            System.out.println("Employee of id " + id + " deleted successfully.");
        } else {
            System.out.println("No employee was deleted.");
        }
    }

    public void update(Employee employee) {
        ObjectId id = new ObjectId(employee.getId());
        Bson filter = Filters.eq("_id", id);
        Document updated = parseEmployeeToDocument(employee);
        this.collection.replaceOne(filter, updated);
    }

    private Employee parseDocumentToEmployee(Document document) {
        Document addressDoc = document.get("address", Document.class);
        Address address = new Address(
                addressDoc.getString("street"),
                addressDoc.getInteger("number"),
                addressDoc.getString("neighborhood"),
                addressDoc.getString("city"),
                addressDoc.getString("state"),
                addressDoc.getString("country"),
                addressDoc.getString("zipCode")
        );

        Employee employee = new Employee(
                document.getString("name"),
                document.getString("phoneNumber"),
                address,
                document.getBoolean("hasDependents")
        );

        employee.setId(document.getObjectId("_id").toString());

        return employee;
    }

    private Document parseEmployeeToDocument (Employee employee) {
        Address address = employee.getAddress();
        Document addressDoc = new Document("street", address.getStreet())
                .append("number", address.getNumber())
                .append("neighborhood", address.getNeighborhood())
                .append("city", address.getCity())
                .append("state", address.getState())
                .append("country", address.getCountry())
                .append("zipCode", address.getZipCode());

        return new Document("name", employee.getName())
                .append("address", addressDoc)
                .append("phoneNumber", employee.getPhoneNumber())
                .append("hasDependents", employee.hasDependents());
    }
}
