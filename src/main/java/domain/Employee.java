package domain;

public class Employee {
    private String id;
    private String name;
    private String phoneNumber;
    private Address address;
    private Boolean hasDependents;

    public Employee(String name, String phoneNumber, Address address, Boolean hasDependents) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.hasDependents = hasDependents;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public Boolean hasDependents() {
        return hasDependents;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("********************************************************\n");
        builder.append("Id: ").append(this.id).append("\n");
        builder.append("Name: ").append(this.name).append("\n");
        builder.append("Phone: ").append(this.phoneNumber).append("\n");
        builder.append("Has Dependents: ").append(this.hasDependents).append("\n");
        builder.append("Street: ").append(this.address.getStreet()).append("\n");
        builder.append("Number: ").append(this.address.getNumber()).append("\n");
        builder.append("Neighborhood: ").append(this.address.getNeighborhood()).append("\n");
        builder.append("City: ").append(this.address.getCity()).append("\n");
        builder.append("State: ").append(this.address.getState()).append("\n");
        builder.append("Country: ").append(this.address.getCountry()).append("\n");
        builder.append("Zip Code: ").append(this.address.getZipCode()).append("\n");
        builder.append("********************************************************\n");

        return builder.toString();
    }

    public void setName(String newName) {
        this.name = newName;
    }
}
